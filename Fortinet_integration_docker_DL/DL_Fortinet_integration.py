from datetime import datetime
import ConfigParser
import requests
import logging
import urllib3
import netaddr
import base64
import time
import json
import sys
import ast
import re

config = ConfigParser.ConfigParser()
config.read("settings.ini")

skip_fg = ast.literal_eval(config.get("skip", "fg"))
skip_fa = ast.literal_eval(config.get("skip", "fa"))
skip_fsa = ast.literal_eval(config.get("skip", "fsa"))

ext_networks = ast.literal_eval(config.get("externalIPRange", "ext_networks"))
int_networks = ast.literal_eval(config.get("internalIPRange", "int_networks"))

log_in_file = ast.literal_eval(config.get("logging", "log_in_file"))
logfile_name = config.get("logging", "logfile_name")
log_level = logging.INFO

s1_hostname = config.get("SentinelOne params", "s1_hostname")
s1_api_token = config.get("SentinelOne params", "s1_api_token")
threat_detected = ast.literal_eval(config.get("SentinelOne params", "threat_detected"))
threat_resolved = ast.literal_eval(config.get("SentinelOne params", "threat_resolved"))
limit = config.get("SentinelOne params", "limit")

fo_hostname = config.get("FortiGate params", "fo_hostname")
fo_username = config.get("FortiGate params", "fo_username")
fo_password = config.get("FortiGate params", "fo_password")
fo_ssl_check = ast.literal_eval(config.get("FortiGate params", "fo_ssl_check"))
fo_computer_name_prefix = config.get("FortiGate params", "fo_computer_name_prefix")
fo_infected_group_name = config.get("FortiGate params", "fo_infected_group_name")

fa_hostname = config.get("FortiAuthenticator params", "fa_hostname")
fa_username = config.get("FortiAuthenticator params", "fa_username")
fa_password = config.get("FortiAuthenticator params", "fa_password")
fa_ssl_check = ast.literal_eval(config.get("FortiAuthenticator params", "fa_ssl_check"))

fsa_hostname = config.get("FortiSandBox params", "fsa_hostname")
fsa_username = config.get("FortiSandBox params", "fsa_username")
fsa_password = config.get("FortiSandBox params", "fsa_password")
fsa_ssl_check = ast.literal_eval(config.get("FortiSandBox params", "fsa_ssl_check"))

if not ast.literal_eval(config.get("SSL", "enabled")):
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def fsa_build_request(method, params, session):
    request = {
        "method": method,
        "params": [
            params
        ],
        "session": session,
        "id": 1,
        "ver": "2.0"
    }
    return request

def fsa_login():
    params = {
        "url": "/sys/login/user",
        "data": [
            {
                "user": fsa_username,
                "passwd": fsa_password
            }
        ]
    }
    r = requests.post(fsa_hostname, data=json.dumps(fsa_build_request("exec", params, None)), verify=fsa_ssl_check)
    if r.json()['result']['status']['message'] != "OK":
        logging.error("Cannot auth in \"%s\"" % fsa_hostname)
        logging.debug(r.json())
        return None
    return r.json()['session']

def fsa_logout(session):
    params = {
        "url": "/sys/logout",
        "data": [
            {
                "user": fsa_username,
                "passwd": fsa_password
            }
        ]
    }
    r = requests.post(fsa_hostname, data=json.dumps(fsa_build_request("exec", params, session)), verify=fsa_ssl_check)
    if r.json()['result']['status']['message'] != "OK":
        logging.error("Cannot logout of \"%s\"" % fsa_hostname)
        logging.debug(r.json())

def fsa_post_blacklist(bl_hash, session):
    params = {
        "url": "/scan/policy/black-white-list",
        "list_type": "black",
        "checksum_type": "sha1",
        "action": "append",
        "upload_file": base64.b64encode(bl_hash)
    }
    r = requests.post(fsa_hostname, data=json.dumps(fsa_build_request("post", params, session)), verify=fsa_ssl_check)
    if r.json()['result']['status']['message'] != "OK":
        logging.error("Cannot post hash \"%s\" in blacklist" % bl_hash)
        logging.debug(r.json())
        return
    logging.info("\"%s\" has been blacklisted" % bl_hash)

def fa_disable_user(username):
    headers = {'Accept': 'application/json'}
    r = requests.get(fa_hostname+'api/v1/localusers', headers=headers, verify=False, auth=(fa_username, fa_password))
    if r.status_code != 200:
        logging.error("Couldn't get localusers.")
    users = r.json()['objects']
    for user in users:
        if user['username'] == username:
            if user['active'] == False:
                logging.info("\"%s\" has been disabled." % username)
                return
            user_uri = user['resource_uri']
            data = {"active": False}
            r = requests.patch(fa_hostname+user_uri, headers=headers, verify=False, auth=(fa_username, fa_password), data=json.dumps(data))
            if r.status_code != 200:
                logging.error("Couldn't disable \"%s\"." % username)
                return
            logging.info("\"%s\" has been disabled." % username)

def fa_enable_user(username):
    headers = {'Accept': 'application/json'}
    r = requests.get(fa_hostname+'api/v1/localusers', headers=headers, verify=False, auth=(fa_username, fa_password))
    if r.status_code != 200:
        logging.error("Couldn't get localusers.")
        return
    users = r.json()['objects']
    for user in users:
        if user['username'] == username:
            if user['active'] == True:
                logging.info("\"%s\" has been enabled." % username)
                return
            user_uri = user['resource_uri']
            data = {"active": True}
            r = requests.patch(fa_hostname+user_uri, headers=headers, verify=False, auth=(fa_username, fa_password), data=json.dumps(data))
            if r.status_code != 200:
                logging.error("Couldn't enable \"%s\"." % username)
                return
            logging.info("\"%s\" has been enabled." % username)

def fo_delete_group(fo_cookies, fo_ccsrf_token, group_name):
    r = requests.get(fo_hostname + "api/v2/cmdb/firewall/addrgrp/" + group_name, cookies=fo_cookies, verify=fo_ssl_check)
    if r.status_code == 200:
        headers = {"X-CSRFTOKEN": fo_ccsrf_token}
        params={'vdom': "root"}
        r = requests.delete(fo_hostname + "api/v2/cmdb/firewall/addrgrp/" + group_name, headers=headers, cookies=fo_cookies, verify=fo_ssl_check, params=params)
        if r.status_code != 200:
            logging.error("Couldn't delete group \"%s\"." % group_name)
            logging.debug(r.json())
            return
    logging.info("\"%s\" group has been removed." % group_name)

def fo_delete_address(fo_cookies, fo_ccsrf_token, address):
    headers = {"X-CSRFTOKEN": fo_ccsrf_token}
    params={'vdom': "root"}
    r = requests.delete(fo_hostname + "api/v2/cmdb/firewall/address/" + address, headers=headers, cookies=fo_cookies, verify=fo_ssl_check, params=params)
    if r.status_code != 200:
        logging.error("Couldn't delete address \"%s\"." % address)
        logging.debug(r.json())
        return
    logging.info("\"%s\" address has been removed." % address)

def fo_add_member(computer_name, fo_cookies, fo_ccsrf_token, group_name):
    headers = {"X-CSRFTOKEN": fo_ccsrf_token }
    params={'vdom': "root"}
    r = requests.get(fo_hostname + "api/v2/cmdb/firewall/addrgrp/" + group_name, cookies=fo_cookies, verify=fo_ssl_check)
    if r.status_code != 200:
        logging.error("Couldn't get addresses in \"%s\" group." % group_name)
        logging.debug(r.json())
        return
    members = r.json()['results'][0]['member']
    tab = []
    data = {}
    tab.append({'q_origin_key': computer_name, 'name': computer_name})
    for member in members:
        tab.append({'q_origin_key': member['q_origin_key'], 'name': member['name']})
        data = {'member': tab}
    if len(members) == 1:
        if members[0]['name'] == 'none':
            data = {'member': [{'q_origin_key': computer_name, 'name': computer_name}]}
    r = requests.put(fo_hostname + "api/v2/cmdb/firewall/addrgrp/" + group_name, headers=headers, cookies=fo_cookies, verify=fo_ssl_check, params=params, data=json.dumps(data))
    if r.status_code != 200:
        logging.error("Couldn't add \"%s\" to the \"%s\" group." % (computer_name, group_name))
        logging.debug(r.json())
        return
    logging.info("\"%s\" has been added to the \"%s\" group." % (computer_name, group_name))

def fo_delete_member(computer_name, fo_cookies, fo_ccsrf_token, group_name):
    headers = {"X-CSRFTOKEN": fo_ccsrf_token }
    params={'vdom': "root"}
    r = requests.get(fo_hostname + "api/v2/cmdb/firewall/addrgrp/" + group_name, cookies=fo_cookies, verify=fo_ssl_check)
    if r.status_code != 200:
        logging.error("Couldn't get addresses in \"%s\" group." % group_name)
        logging.debug(r.json())
        return
    members = r.json()['results'][0]['member']
    tab = []
    data = {}
    for member in members:
        if member['name'] == computer_name:
            continue
        tab.append({'q_origin_key': member['q_origin_key'], 'name': member['name']})
        data = {'member': tab}
    if len(data) == 0:
        data = {'member': [{'q_origin_key': 'none', 'name': 'none'}]}
    r = requests.put(fo_hostname + "api/v2/cmdb/firewall/addrgrp/" + group_name, headers=headers, cookies=fo_cookies, verify=fo_ssl_check, params=params, data=json.dumps(data))
    if r.status_code != 200:
        logging.error("Couldn't remove \"%s\" from the \"%s\" group." % (computer_name, group_name))
        logging.debug(r.json())
        return
    logging.info("\"%s\" has been removed from the \"%s\" group." % (computer_name, group_name))

def fo_create_group(fo_cookies, fo_ccsrf_token, group_name):
    r = requests.get(fo_hostname + "api/v2/cmdb/firewall/addrgrp/" + group_name, cookies=fo_cookies, verify=fo_ssl_check)
    if r.status_code != 200:
        headers = {"X-CSRFTOKEN": fo_ccsrf_token}
        params={'vdom': "root"}
        data = {'name': group_name, 'member': [{'name':"none"}]}
        r = requests.post(fo_hostname + "api/v2/cmdb/firewall/addrgrp", headers=headers, cookies=fo_cookies, verify=fo_ssl_check, data=json.dumps(data), params=params)
        if r.status_code != 200:
            logging.warning("Cannot create address group \"%s\"" % group_name)
            logging.debug(r.json())
        return

def fo_create_address(computer_name, agent_ip, agent_netmask, fo_cookies, fo_ccsrf_token):
    headers = {"X-CSRFTOKEN": fo_ccsrf_token }
    params={'vdom': "root"}
    data={'name': computer_name,
          'subnet': agent_ip + " " + agent_netmask}
    r = requests.post(fo_hostname + "api/v2/cmdb/firewall/address", headers=headers, cookies=fo_cookies, verify=fo_ssl_check, data=json.dumps(data), params=params)
    if r.status_code != 200:
        logging.warning("Cannot create address \"%s\"" % computer_name)
        logging.debug(r.json())
        return

def fo_check_address_exist(computer_name, fo_cookies):
    addresses = fo_get_all_addresses(fo_cookies)
    for address in addresses:
        if computer_name in address['name']:
            return True
    return False

def fo_get_all_addresses(fo_cookies):
    r = requests.get(fo_hostname + "api/v2/cmdb/firewall/address", cookies=fo_cookies, verify=fo_ssl_check)
    if r.status_code != 200:
        logging.warning("Couldn't get all addresses.")
        logging.debug(r.json())
        return None
    return r.json()['results']

def fo_login_get_cookie():
    r = requests.post(fo_hostname + "logincheck", data={"username": fo_username, "secretkey": fo_password}, verify=fo_ssl_check)
    if r.status_code != 200:
        logging.warning("Cannot auth in \"%s\"" % fo_hostname)
        logging.debug(r.json())
        return None
    return r.cookies

def check_ip_network(agent_ip, networks):
    agent_ipp = netaddr.IPAddress(agent_ip).value
    for netw in networks:
        network = netaddr.IPNetwork(netw)
        if agent_ipp >= network.first and agent_ipp <= network.last:
            return True
    return False



def main_loop():
    last_activity_ts = None
    s1_headers = {
        "Content-type": "application/json",
        "Authorization": "APIToken " + s1_api_token
    }
    while True:
        s1_params = {
            "createdAt__gt": last_activity_ts,
            "sortOrder": "desc",
            "limit": limit
        }
        r = requests.get(s1_hostname+'/web/api/v2.0/activities', headers=s1_headers, params=s1_params)
        if r.status_code != 200:
            print "Error: %s" % r.json()
            sys.exit("Couldn't get activities. Exiting.")
        activities = r.json()['data']
        if activities:
            logging.info("--- New datas ---")
            last_activity_ts = activities[0]['createdAt']
            for activity in activities:
                logging.info("==> time:" + activity['createdAt'])
                logging.info("=> type:" + str(activity['activityType']))
                if activity['activityType'] in threat_detected:
                    bl_hash = activity['data']['fileContentHash']
                    agent_id = activity['agentId']
                    s1_params2 = {
                        "ids": agent_id
                    }
                    ret = requests.get(s1_hostname+'/web/api/v2.0/agents', headers=s1_headers, params=s1_params2)
                    if ret.status_code != 200:
                        print "Error: %s" % ret.json()
                        sys.exit("Couldn't get agent informations. Exiting.")
                    agent = ret.json()['data'][0]
                    agent_external_ip = agent['externalIp']
                    last_username = agent['lastLoggedInUserName']
                    computer_name = fo_computer_name_prefix + agent['computerName']
                    interfaces = agent['networkInterfaces']
                    logging.info('IP: ' + agent_external_ip + ' - User: ' + last_username + ' - Computer: ' + computer_name)
                    logging.info('New threat')
                    if not skip_fg:
                        fo_cookies = fo_login_get_cookie()
                        fo_ccsrf_token = fo_cookies.get_dict()['ccsrftoken'].strip('"')
                        fo_create_group(fo_cookies, fo_ccsrf_token, fo_infected_group_name)

                        if not check_ip_network(agent_external_ip, ext_networks):
                            logging.info("External IP \"%s\" not in the range provided, no actions taken on this IP", agent_external_ip)
                        else:
                            fo_create_group(fo_cookies, fo_ccsrf_token, computer_name)
                            tmp = computer_name+'-'+str(agent_external_ip)
                            if not fo_check_address_exist(tmp, fo_cookies):
                                fo_create_address(tmp, agent_external_ip, "255.255.255.255", fo_cookies, fo_ccsrf_token)
                            fo_add_member(tmp, fo_cookies, fo_ccsrf_token, computer_name)

                        for interface in interfaces:
                            int_ips = interface['inet']
                            for int_ip in int_ips:
                                if not check_ip_network(int_ip, int_networks):
                                    logging.info("Interface IP not in the range provided, no actions taken on this IP")
                                else:
                                    tmp = computer_name+'-'+str(int_ip)
                                    if not fo_check_address_exist(tmp, fo_cookies):
                                        fo_create_address(tmp, int_ip, "255.255.255.255", fo_cookies, fo_ccsrf_token)
                                    fo_create_group(fo_cookies, fo_ccsrf_token, computer_name)
                                    fo_add_member(tmp, fo_cookies, fo_ccsrf_token, computer_name)
                        fo_add_member(computer_name, fo_cookies, fo_ccsrf_token, fo_infected_group_name)

                    if not skip_fa:
                        fa_disable_user(last_username)
                    if not skip_fsa:
                        fsa_session = fsa_login()
                        fsa_post_blacklist(bl_hash, fsa_session)
                        fsa_logout(fsa_session)

                elif activity['activityType'] in threat_resolved:
                    threat_id = activity['threatId']
                    s1_params3 = {
                        "ids": threat_id
                    }
                    ret_t = requests.get(s1_hostname+'/web/api/v2.0/threats', headers=s1_headers, params=s1_params3)
                    if ret_t.status_code != 200:
                        print "Error: %s" % ret_t.json()
                        sys.exit("Couldn't get threat informations. Exiting.")
                    threat = ret_t.json()['data'][0]
                    agent_id = threat['agentId']
                    s1_params4 = {
                        "ids": agent_id
                    }
                    ret_a = requests.get(s1_hostname+'/web/api/v2.0/agents', headers=s1_headers, params=s1_params4)
                    if ret_a.status_code != 200:
                        print "Error: %s" % ret_a.json()
                        sys.exit("Couldn't get threat informations. Exiting.")
                    agent = ret_a.json()['data'][0]

                    agent_external_ip = agent['externalIp']
                    last_username = agent['lastLoggedInUserName']
                    computer_name = fo_computer_name_prefix + agent['computerName']
                    logging.info('IP: ' + agent_external_ip + ' - User: ' + last_username + ' - Computer: ' + computer_name)
                    if not agent['infected']:
                        logging.info('No more threats on endpoint, moving back to healthy group')
                        if not skip_fg:
                            fo_cookies = fo_login_get_cookie()
                            fo_ccsrf_token = fo_cookies.get_dict()['ccsrftoken'].strip('"')
                            fo_create_group(fo_cookies, fo_ccsrf_token, fo_infected_group_name)
                            fo_delete_member(computer_name, fo_cookies, fo_ccsrf_token, fo_infected_group_name)
                            fo_delete_group(fo_cookies, fo_ccsrf_token, computer_name)
                            all_addr = fo_get_all_addresses(fo_cookies)
                            for addr in all_addr:
                                if computer_name in addr['q_origin_key']:
                                    fo_delete_address(fo_cookies, fo_ccsrf_token, addr['q_origin_key'])
                        if not skip_fa:
                            fa_enable_user(last_username)
                    else:
                        logging.info('There are still threats on endpoint, keeping it in infected group.')

                else:
                    logging.info("No actions performed on this activity type.")

        time.sleep(1)

def setup_logging():
    if logging.root:
        del logging.root.handlers[:]
    if log_in_file:
        logging.basicConfig(filename=logfile_name, format='%(asctime)s [%(levelname)-7.7s] %(message)s', level=log_level)
    else:
        logging.basicConfig(format='%(asctime)s [%(levelname)-7.7s] %(message)s', level=log_level)
    return

if __name__ == "__main__":
    if len(sys.argv) != 1:
        sys.exit("Usage: python DL_Fortinet_integration.py")
    setup_logging()
    main_loop()